import { configureStore } from '@reduxjs/toolkit'
import friendReducer from "../features/friendType/friendType"

export default configureStore({
  reducer: {
    status: friendReducer
  },
})