import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link, Routes, } from "react-router-dom";
import Sidebar from './component/Sidebar'
import MainPage from './pages/mainPage/';
import FireShip from "./pages/fireShip/"
import ChatList from './component/ChatList';
import Nitro from './component/Nitro';


function App() {
  return (
    <div className='flex 
    w-screen h-screen pt-8 pl-20
     bg-gray-900
     '>
      <Router>
        <Sidebar />
        <Routes>
          <Route path='/main-page' element={<MainPage />} >
            <Route path='/main-page/friends' element={<ChatList />} />
            <Route path='/main-page/nitro' element={<Nitro />} />
          </Route>
          <Route path='/fireShip' element={<FireShip />} />
        </Routes>
      </Router>
    </div>
  );
}


export default App;
