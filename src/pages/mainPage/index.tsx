import { useEffect, useState } from "react";
import Heading from "../../component/Heading";
import Chat from "../../component/Chat";
import FriendChat from "../../component/FriendChat";
import { FaUserFriends, FaPlus } from "react-icons/fa"
import { BsSpeedometer } from "react-icons/bs";
import avatar1 from "../../img/avatar/1.png"
import avatar2 from "../../img/avatar/2.jpg"
import avatar3 from "../../img/avatar/3.jpg"
import avatar4 from "../../img/avatar/4.webp"
import { Link, Outlet } from "react-router-dom";
import { useSelector } from "react-redux";

const friendList: any = [
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
  {
    avatar: avatar1,
    status: "Online",
    name: "Musk",
    id: "musk2034"
  },
  {
    avatar: avatar3,
    status: "Idle",
    name: "Nova",
    id: "super"
  },
  {
    avatar: avatar2,
    status: "Offline",
    name: "Easy",
    id: "#74957"
  },
]

function MainPage() {
  const friendStatus = useSelector((state: any) => state.status.value)
  const [dataByHeading, setDataByHeading] = useState(friendList)

  useEffect(() => {
    if (friendStatus === "All") {
      setDataByHeading(friendList)
    } else {
      let filteredFriend = friendList.filter((friend: any) =>
        friend.status === friendStatus)
      setDataByHeading(filteredFriend)
    }
  }, [friendStatus])

  return (
    <div className='text-white w-full h-full bg-gray-800 
    flex
    '>
      <div className=" bg-gray-800 h-full flex rounded-tl-lg">
        <ChatList />
      </div>
      <div className="w-full h-full flex flex-col">
        <Heading />
        <Outlet context={dataByHeading} />
      </div>
    </div>
  )
}

const ChatList = () => {
  return (
    <div className="w-72 bg-gray-800 rounded-tl-lg flex flex-col relative">
      <div className='bg-gray-700 h-14 min-h-[3.5rem] rounded-tl-lg flex border-b-[1px] border-gray-900'>
        <FindChat />
      </div>
      <div>
        <Link to="/main-page/friends">
          <Chat content="Friends"
            icon={<FaUserFriends size={20} />}
          />
        </Link>
        <Link to="/main-page/nitro">
          <Chat content="Nitro"
            icon={<BsSpeedometer size={20} />}
          />
        </Link>
      </div>
      <div className="flex items-center mx-6 my-4 justify-between
      text-sm font-bold text-gray-500 hover:text-gray-400
      ">
        DIRECT MESSAGES
        <FaPlus />
      </div>
      <div className="h-full overflow-y-scroll scrollbar 
      hover:overflow-y-scroll hover:thin-scrollbar">
        {friendList.map((friend: any, index: any) => {
          return (
            <FriendChat
              key={index}
              avatar={friend.avatar}
              name={friend.name}
              status={friend.status}
              label="name"
            />
          )
        }
        )}
      </div>
      <div className="absolute bottom-0 h-min bg-gray-800 w-full ">
        <FriendChat
          avatar={avatar4}
          name={"Hugh"}
          status={"online"}
          id={"Hugh#1213"}
          className=" px-1 max-h-[50px] rounded-none bg-gray-850"
        />
      </div>
    </div>
  )
}

const FindChat = () => {
  return (
    <div className="w-72 min-w-[18rem] bg-gray-800 px-2 flex items-center rounded-tl-lg">
      <input
        type="text"
        className="bg-gray-900 w-full h-8 rounded-md
        placeholder:p-2 placeholder:text-[#ccccccb8] focus:border-transparent focus:outline-none"
        placeholder="Find or start a conversation"
      />
    </div>
  )
}

export default MainPage;