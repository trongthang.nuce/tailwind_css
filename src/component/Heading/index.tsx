import { useState, useRef } from "react";
import { FaUserFriends } from "react-icons/fa"
import { BiSolidMessageAdd, BiDownload, BiSolidInbox, BiSolidHelpCircle } from "react-icons/bi"
import { IconContext } from 'react-icons';
import CustomIcon from "../CustomIcon";
import { friendStatusChange } from "../../features/friendType/friendType";
import { useDispatch } from "react-redux";
function Heading(props: any) {

  return (
    <div className='bg-gray-700 h-14 min-h-[3.5rem]  flex border-b-[1px] border-gray-900 w-full'>
      <div className="flex items-center justify-between w-[-webkit-fill-available]">
        <Friends/>
        <Unity />
      </div>
    </div>
  )
}

const Friends = (props: any) => {
  const dispatch = useDispatch()

  const FriendsOption = [
    "Online",
    "All",
    "Idle",
    "Offline",
    "Add Friends",
  ]
  // document.getElementById(status)!.style.color = "green";

  const handleChange = (event: any) => {
    // console.log(event);
    FriendsOption.forEach(option => {
      if (event === option && event !== "Add Friends") {
        document.getElementById(event)!.classList.add("status-active");
      } else if (event !== option) {
        document.getElementById(option)!.classList.remove("status-active")
        document.getElementById("Add Friends")!.style.color = "white";
        document.getElementById("Add Friends")!.style.background = "#15803d";
      } else {
        document.getElementById("Add Friends")!.style.color = "#4ade80";
        document.getElementById("Add Friends")!.style.background = "#36393f";
      }
    })
    dispatch(friendStatusChange(event))
  };

  return (
    <div className="flex items-center">
      <div className="bg-gray-700 flex items-center m-2 px-4 gap-2 
      border-r-[1px] border-gray-600 font-bold text-gray-200
      ">
        <FaUserFriends size={20} />Friends
      </div>
      {FriendsOption.map((friend, index) => {
        return (
          <button
            key={index}
            id={friend}
            className="
            px-2 py-1 mx-4 min-w-max text-gray-400 bg-gray-700 rounded-lg
          last:bg-green-700 last:text-gray-100 
            font-bold text-base
            hover:text-gray-300 hover:bg-gray-600 hover:last:bg-green-700 hover:last:text-white
            "
            onClick={() => { handleChange(friend) }}
          >
            {friend}
          </button>
        )
      })}
      {/* <div>
        <input type="file" className="block w-full text-sm text-slate-500
      file:mr-4 file:py-2 file:px-4
      file:rounded-full file:border-0
      file:text-sm file:font-semibold
      file:bg-violet-50 file:text-violet-700
      hover:file:bg-violet-100
    "/>
      </div> */}
    </div>
  )
}

const Unity = () => {
  const iconsData = [
    { component: BiDownload, popup: "bottom", size: "xl", popupMes: "Download" },
    { component: BiSolidInbox, popup: "bottom", size: "xl", popupMes: "Inbox" },
    { component: BiSolidHelpCircle, popup: "bottom", size: "xl", popupMes: "Help" },
    // ...more icons
  ];
  return (
    <div className="flex">
      <div className="px-6 border-r-[1px] border-gray-600 m-auto">
        <CustomIcon
          icon={BiSolidMessageAdd}
          popup="bottom"
          size="xl"
          popupMes="New group DM"
          className={"p-0 pt-3"}
        />
      </div>
      <IconContext.Provider value={{}}>
        <div className="flex">
          {iconsData.map((icon: any, index: number) => {
            return (
              <CustomIcon
                key={index}
                icon={icon.component}
                popup={icon.popup}
                size={icon.size}
                popupMes={icon.popupMes}
              />
            )
          })}
        </div>
      </IconContext.Provider>
      <div>

      </div>
    </div>
  )
}

export default Heading;