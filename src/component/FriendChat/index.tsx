import Status from "../Status";
import { IoChatbox } from "react-icons/io5"
import { BiSolidMicrophone } from "react-icons/bi";
import { RiSettings5Fill } from "react-icons/ri"
import { SlOptionsVertical } from "react-icons/sl"
import { MdHeadset } from "react-icons/md"
import Icon from "../CustomIcon";

type STATUS = "online" | "offline" | "idle";
type LABEL = "name" | "full" | "empty";

interface Props {
  avatar?: any;
  status?: STATUS;
  name?: string;
  label?: LABEL;
  className?: string;
  id?: string;
}


const FriendChat = (props: Props) => {
  const { avatar, status, name, label, className, id } = props;
  const chatIcon = IoChatbox;
  const optionIcon = SlOptionsVertical;


  return (label === "name") ?
    (
      <div
        className={`chat-item mx-0 gap-3 ${className} `}
      >
        <div className={`relative`}>
          <img
            alt="avatar"
            src={avatar}
            className="w-10 h-10 rounded-full relative"
          />
          <Status data={status} />
        </div>
        {name}
      </div >
    ) : (label === "full") ?
      (
        <div className={`group chat-item-full mx-0 gap-3 ${className}`}>
          <div className="flex h-20 mx-2 items-center gap-5 text-lg w-full border-t-[1px] border-gray-600">
            <div className={`relative`}>
              <img
                alt="avatar"
                src={avatar}
                className=" w-10 h-10 rounded-full relative"
              />
              <Status data={status} />
            </div>
            <div className="flex flex-col pl-1">
              <div className=" text-white font-bold">
                {name}
                <span className=" hidden group-hover:inline-block ml-2 font-thin text-gray-500">
                  {id}
                </span>
              </div>
              <div className=" text-[14px]">
                {status}
              </div>
            </div>
            <div className="flex ml-auto gap-3 ">
              <Icon
                icon={chatIcon}
                popupMes="Message"
                popup="top"
                size={"md"}
                className={"bg-gray-800"}
              />
              <Icon
                icon={optionIcon}
                popupMes="More"
                popup="top"
                size={"md"}
                className={"bg-gray-800"}
              />
            </div>
          </div>
        </div >
      ) : (
        <div className={`group chat-item-user mx-0 gap-3 ${className}`}>
          <div className="flex mx-2 items-center gap-1 w-full hover:bg-gray-600 rounded-md">
            <div className={`relative`}>
              <img
                alt="avatar"
                src={avatar}
                className=" w-10 h-10 rounded-full relative"
              />
              <Status data={status} />
            </div>
            <div className="pl-1 leading-3 ">
              <div className=" text-gray-200 font-bold text-base my-auto ">
                {name}
                <div className="font-thin text-gray-500 text-sm">
                  {id}
                </div>
              </div>
            </div>
          </div>
          <div className="flex ml-auto mr-2">
            <div className="icon-user"><BiSolidMicrophone size={20} /></div>
            <div className="icon-user"><MdHeadset size={20} /></div>
            <div className="icon-user"><RiSettings5Fill size={20} /></div>
          </div>
        </div >
      )
}

export default FriendChat;