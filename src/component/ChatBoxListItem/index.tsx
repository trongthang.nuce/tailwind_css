import { IoChatbox } from "react-icons/io5"
import { SlOptionsVertical } from "react-icons/sl"
import FriendChat from "../../component/FriendChat";

interface Props {
  data?: any;
  avatarSize?: any;
}

function ChatBoxListItem(prop: Props) {
  const { data, avatarSize } = prop;
  return (
    <div>
      <div>
        {data?.map((friend: any, index: number) => {
          return (
            <FriendChat
              key={index}
              avatar={friend.avatar}
              name={friend.name}
              status={friend.status}
              id={friend.id}
              label="full"
              className=" px-2 "
            />
          )
        }
        )}
      </div>
    </div>
  )
}

export default ChatBoxListItem;