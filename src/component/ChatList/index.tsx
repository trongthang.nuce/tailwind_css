import { BiSearch, } from "react-icons/bi";
import { AiOutlineClose } from "react-icons/ai"
import ChatBoxListItem from "../ChatBoxListItem"
import { useOutletContext } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector } from 'react-redux'
interface Props {
  type?: string;
}

function ChatList(prop: Props) {
  const friends: any = useOutletContext();
  const [search, setSearch] = useState("")
  const [renderFriend, setRenderedFriend] = useState()
  const status = useSelector((state: any) => state.status.value)

  const handleSearch = (res: any) => {
    setSearch(res)
  }

  useEffect(() => {
    let filteredFriend = friends.filter((friend: any) =>
      (friend.name.toLowerCase()).includes(search.toLowerCase()))
    setRenderedFriend(filteredFriend)
  }, [search, friends])

  // console.log(renderFriend);

  return (
    <div className="flex w-full h-[-webkit-fill-available] min-h-0">
      <div className=" w-3/4 bg-gray-700 py-4 flex flex-col">
        <div className="relative mx-8">
          <input type="text"
            className="w-full h-10 rounded-md bg-gray-900 
          placeholder:text-gray-500 pl-3 placeholder:text-lg
          relative focus:border-transparent focus:outline-none"
            placeholder="Search"
            value={search}
            onChange={(e) => handleSearch(e.target.value)}
          />
          {search.length == 0 ?
            <BiSearch size={20}
              className="text-gray-500 absolute top-2 right-2" />
            :
            <AiOutlineClose size={20}
              className="text-gray-500 absolute top-2 right-2"
              onClick={() => setSearch("")}
            />
          }
        </div>
        <div className="text-gray-400 font-bold text-sm my-6 mx-8">
          {status.toUpperCase()} - {friends.length}
        </div>
        <div className="overflow-y-scroll thin-scrollbar ml-6 pr-4 mr-2">
          <ChatBoxListItem
            data={renderFriend}
            avatarSize={"big"}
          />
        </div>
      </div>
      <div className="h-full bg-gray-700 w-1/4 min-w-[380px] border-l-[1px] border-l-gray-600">
        <div className=" text-2xl font-bold px-4 py-6">
          Active now
        </div>
        <div className=" text-lg font-bold text-center mb-2">
          It's quiet for now...
        </div>
        <div className=" text-gray-500 text-center px-10">
          When a friend starts an activity--like playing a game or hanging out on voice--we'll show it here!
        </div>
      </div>
    </div>
  )
}

export default ChatList;