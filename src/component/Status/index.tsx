interface Props {
  data?: string;
}

function Status(prop: Props) {
  const { data } = prop;
  return (
    <div>
      {
        data === "Online" ?
          <div className="status absolute right-0 bottom-[-4px] border-gray-800 border-[3px] rounded-full 
          h-[15px] w-[15px] bg-green-600">
          </div>
          : data === "Offline" ?
            <div className="status absolute right-0 bottom-[-4px] border-gray-800 border-[3px] rounded-full 
              h-[15px] w-[15px] bg-gray-500 flex items-center justify-center">
              <div className="bg-gray-800 h-[5px] w-[5px] rounded-full"></div>
            </div> :
            <div className="status absolute right-0 bottom-[-4px] border-gray-800 border-[3px] rounded-full 
              h-[15px] w-[15px] bg-yellow-500 flex items-start justify-start">
              <div className="bg-gray-800 h-[5.5px] w-[5.5px] rounded-full"></div>
            </div>
      }

    </div>
  )
}

export default Status;