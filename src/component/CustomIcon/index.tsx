import React from 'react';
import { IconType } from 'react-icons';
import { Popover } from 'antd';
import "./index.css";

type ICONSIZE = "xs" | "sm" | "md" | "lg" | "xl";
type POPUPPOSITION = "top" | "bottom" | "none";

interface Props {
  icon: IconType;
  size?: ICONSIZE;
  popup?: POPUPPOSITION;
  popupMes?: string
  props?: string;
  className?: any;
}

function IconBase({
  icon,
  size = "md",
  className,
  popup,
  popupMes,
  ...props
}: Props) {
  const iconStyle = {
    xs: "text-[14px] p-3 leading-[14px] rounded-full text-gray-400 hover:text-gray-200",
    sm: "text-[16px] p-3 leading-[18px] rounded-full text-gray-400 hover:text-gray-200",
    md: "text-[18px] p-3 leading-[18px] rounded-full text-gray-400 hover:text-gray-200",
    lg: "text-[20px] p-3 leading-[20px] rounded-full text-gray-400 hover:text-gray-200",
    xl: "text-[22px] p-3 leading-[21px] rounded-full text-gray-400 hover:text-gray-200",
  }
  return (popup === "none" || popup === undefined) ? (
    <div className={`${iconStyle[size]} ${className}`} {...props}>
      {React.createElement(icon)}
    </div>
  ) : (
    <Popover
      title={`${popupMes}`}
      placement={`${popup}`}
      overlayClassName='popup'
    >
      <div className={`${iconStyle[size]} ${className}`} {...props}>
        {React.createElement(icon)}
      </div>
    </Popover>
  );
}

const CustomIcon: React.FC<Props> = ({
  icon, props, popupMes, popup, className, size
}: Props) => {

  return (
    <IconBase
      icon={icon}
      popup={popup}
      popupMes={popupMes}
      props={props}
      className={className}
      size={size}
    />
  )
  // return (popup === "none" || popup === undefined) ? (
  //   <div className={`${props}`}>
  //     {React.createElement(icon)}
  //   </div>
  // ) : (
  //   <Popover
  //     title={`${popupMes}`}
  //     placement={`${popup}`}
  //     overlayClassName='popup'
  //   >
  //     <div className={`${props}`}>
  //       {React.createElement(icon)}
  //     </div>
  //   </Popover>
  // );
}

export default CustomIcon;