import { IconType } from "react-icons";
import { FaUserFriends } from "react-icons/fa"

interface Props {
  content?: string;
  icon?: any;
}

function Chat(prop: Props) {
  const { content, icon } = prop;
  return (
    <div className="chat-item ">
      {icon}
      {content}
    </div>
  )
}

export default Chat;