interface Props {
}

function Nitro(prop: Props) {
  return (
    <div>
      <form className="flex items-center space-x-6">
        <div className="shrink-0">
          <img className="h-16 w-16 object-cover rounded-full" src="https://images.unsplash.com/photo-1580489944761-15a19d654956?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1361&q=80" alt="Current profile photo" />
        </div>
        <label className="block">
          <span className="sr-only">Choose profile photo</span>
          <input type="file" className="block w-full text-sm text-slate-500
      file:mr-4 file:py-2 file:px-4
      file:rounded-full file:border-0
      file:text-sm file:font-semibold
      file:bg-violet-50 file:text-violet-700
      hover:file:bg-violet-100
    "/>
        </label>
      </form>

      <ul role="list" className="marker:text-sky-400 list-disc pl-5 space-y-3 text-slate-400">
        <li>5 cups chopped Porcini mushrooms</li>
        <li>1/2 cup of olive oil</li>
        <li>3lb of celery</li>
      </ul>

      <div className="selection:bg-fuchsia-300 selection:text-fuchsia-900">
        <p>
          So I started to walk into the water. I won't lie to you boys, I was
          terrified. But I pressed on, and as I made my way past the breakers
          a strange calm came over me. I don't know if it was divine intervention
          or the kinship of all living things but I tell you Jerry at that moment,
          I <em>was</em> a marine biologist.
        </p>
      </div>

      <p className="first-line:uppercase first-line:tracking-widest
  first-letter:text-7xl first-letter:font-bold first-letter:text-white
  first-letter:mr-3 first-letter:float-left
">
        Well, let me tell you something, funny boy. Y'know that little stamp, the one
        that says "New York Public Library"? Well that may not mean anything to you,
        but that means a lot to me. One whole hell of a lot.
      </p>
    </div>
  )
}

export default Nitro;