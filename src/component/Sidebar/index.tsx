import { useState } from 'react';
import { BsPlus, BsFillLightningFill, BsDiscord } from 'react-icons/bs';
import { FaFire, FaPoo } from 'react-icons/fa';
// import { redirect } from "react-router-dom";
import { BrowserRouter as Router, Route, Link, Routes, } from "react-router-dom";

const text = [
  'fireships',
  'lighting',
  'shit',
  'Add a Server',
]

function Sidebar() {
  return (
    <div className=" pt-2
    fixed top-[0px] left-0 h-screen w-20 m-0 
    flex flex-col bg-gray-900 text-white shadow-lg
    ">
      <CenterItems childElement={
        <div className='scale-y-75 text-gray-400 font-bold text-md'>Discord</div>
      } />
      <Link to='/main-page'>
        <SideBarIcon icon={<BsDiscord size="28" />} text={"Direct Messages"} />
      </Link>

      <CenterItems childElement={
        <div className='h-[1.5px] bg-gray-600 w-8'></div>
      } />
      <Link to='/fireShip'>
        <SideBarIcon icon={<FaFire size="28" />} text={text[0]} />
      </Link>
      <SideBarIcon icon={<BsFillLightningFill size="20" />} text={text[1]} />
      <SideBarIcon icon={<FaPoo size="20" />} text={text[2]} />
      <SideBarIcon icon={<BsPlus size="32" />} text={text[3]} />
    </div>
  )
}

const CenterItems = ({ childElement }: any) => {
  return (
    <div className='flex items-center justify-center'>
      {childElement}
    </div>
  )
}

const SideBarIcon = ({ icon, text }: any) => (
  <div className="sidebar-icon group">
    {icon}
    <span className='sidebar-tooltip group-hover:scale-100'>
      {text}
    </span>
  </div>
)

export default Sidebar;