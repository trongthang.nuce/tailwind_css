import { createSlice } from "@reduxjs/toolkit";

export const friendSlice = createSlice({
  name: "friend",
  initialState: {
    value: "All",
  },
  reducers: {
    friendStatusChange: (state: any, action: any) => {
      state.value = action.payload
    },
  }
})

export const {friendStatusChange} = friendSlice.actions
export default friendSlice.reducer
