const colors = require("tailwindcss/colors")

module.exports = {
  mode: 'jit',
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        primary: '#202225',
        secondary: '#5865f2',
        gray: {
          900: '#202225',
          850: '#282a2e',
          800: '#2f3136',
          700: '#36393f',
          600: '#4f545c',
          500: '#8d97a6',
          400: '#d4d7dc',
          300: '#e3e5e8',
          200: '#ebedef',
          100: '#f2f3f5',
        },
      }
    },
  },
  plugins: [
    require('tailwind-scrollbar'),
  ],
}